Connected Bootstrap 4 Grid. Available classes of:

- <a href='http://aitomic-docs.aitom.cz/base/layout'>Layout</a> (.container, .row, .col-*, .no-gutters, .order-*)
- <a href='http://aitomic-docs.aitom.cz/base/flex'>Flex utilities</a> (.d-flex, .align-*, . justify-*)