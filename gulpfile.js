const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const eslint = require('gulp-eslint');
const fs = require('fs');
const livereload = require('gulp-livereload');
const magicImporter = require('node-sass-magic-importer');
const path = require('path');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const webpack = require('webpack-stream');

gulp.task('default', function() {
	gulp.start('styles');
	gulp.start('scripts');
});

gulp.task('styles', ['styles-lint'], function() {
	return gulp.src(['src/begin.scss', 'src/end.scss'])
			.pipe(concat('style.css'))
			.pipe(plumber({
				errorHandler: function(err) {
					console.log(err);
					this.emit('end');
				}
			}))
			.pipe(sass({
				includePaths: ['node_modules'],
				importer: [
					function (url, prev, done) {
						if (url.indexOf('~') == 0 && path.extname(url) === '.css' &&
							fs.existsSync(nodeUrl = 'node_modules/' + url.replace('~', '')))
							return done({ contents: fs.readFileSync(nodeUrl, 'utf8') });
						return null;
					},
					magicImporter()
				]
			}))
			.pipe(autoprefixer())
			.pipe(cleanCss({
				compatibility: 'ie10',
				inline: ['local']
			}))
			.pipe(gulp.dest('dist'))
			.pipe(livereload());
});

gulp.task('styles-lint', function() {
	return gulp.src('src/**/*.scss')
			.pipe(sassLint({
				configFile: 'config-sass-lint.yml'
			}))
			.pipe(sassLint.format())
			.pipe(sassLint.failOnError());
});

gulp.task('scripts', ['scripts-lint'], function() {
	return gulp.src('src/dev.js')
			.pipe(webpack({
				output: {
					filename: 'script.js'
				},
				mode: 'production',
				module: {
					rules: [
						{
							use: {
								loader: 'babel-loader',
								options: {
									presets: ['@babel/preset-env']
								}
							}
						}
					]
				}
			}))
			.pipe(uglify())
			.pipe(gulp.dest('dist'))
			.pipe(livereload());
});

gulp.task('scripts-lint', function() {
	return gulp.src(['src/**/*.js', '!src/prod.js'])
		.pipe(eslint(require('./es-lint').get))
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task('watch', function() {
	gulp.start('styles');
	gulp.start('scripts');

	watch('src/**/*.scss', { usePolling: true }, function() {
		gulp.start('styles');
	});

	watch('src/**/*.js', { usePolling: true }, function() {
		gulp.start('scripts');
	});

	livereload.listen();
});

module.exports = gulp;