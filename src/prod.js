import jquery from 'jquery';

require('jquery-matchmedia/dist/jquery.matchMedia');

import 'feature.js';

import 'focus-visible';

import nette from 'aitom-nette';

import AutoFocus from 'aitomic-base/src/js/auto-focus';

import EqualRow from 'aitomic-base/src/js/equal-row';

export default class Base {
	constructor() {
		if (typeof(window.feature) !== 'undefined') {
			window.feature.testAll();
		}

		new AutoFocus();

		new EqualRow();

		nette.ext('equal-row', {
			success: () => {
				new EqualRow();
			}
		});
	}
}
