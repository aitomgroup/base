import 'itemslide/dist/itemslide';

(function($) {
	$.fn.defaultCarousel = function(options) {
		var config = $.extend({
			wrap: '[data-carousel="wrap"]',
			stage: '[data-carousel="stage"]',
			items: '[data-carousel="stage"] > *',
			prev: '[data-carousel="nav"][data-direction="prev"]',
			next: '[data-carousel="nav"][data-direction="next"]',
			dots: '[data-carousel="dots"]',
			duration: 350,
			swipe_sensitivity: 150,
			disable_slide: false,
			disable_clicktoslide: true,
			disable_scroll: false,
			disable_dots: false,
			dots_template: '',
			start: 0,
			one_item: false,
			pan_threshold: 0.3,
			swipe_out: false,
			left_sided: true,
			infinite: false,
			autoplay: false,
			autoplay_speed: 5000
		}, options);

		config.disable_autowidth = true;
		config.parent_width = false;

		var defaultCarousel = function($carousel) {
			var $el = {
				carousel: $carousel,
				wrap: $carousel.find(config.wrap),
				stage: $carousel.find(config.stage),
				items: $carousel.find(config.items),
				prev: $carousel.find(config.prev),
				next: $carousel.find(config.next)
			};

			var testInit = function() {
				return $el.items.filter('.itemslide-active').length ? true : false;
			};

			var prepareStage = function() {
				$el.stage.css({
					'-webkit-transform-style': 'preserve-3d',
					'-ms-transform-style': 'preserve-3d',
					'transform-style': 'preserve-3d'
				});

				$el.wrap.css('overflow', 'hidden');
			}; prepareStage();

			var fixGutters = function() {
				if (!$el.stage.hasClass('no-gutters')) {
					var gutterLeft,
						gutterRight = gutterLeft = parseInt($el.items.first().css('paddingLeft'));

					$el.stage.addClass('no-gutters');

					if ($el.stage.hasClass('line-gutters')) {
						$el.stage.removeClass('line-gutters');

						gutterRight = 0;
					}

					$el.items.wrapInner('<div data-carousel="itemInner"style="padding-left: ' +
										gutterLeft + 'px; padding-right: ' +
										gutterRight + 'px; height: 100%"></div>');
				}
			}; fixGutters();

			var size = {
				visible: 0,
				total: $el.items.length - 1
			};

			var setSizes = function() {
				size.viewport = $el.wrap.width();
				size.temp = 0;

				$el.items.each(function(index) {
					var itemWidth = $(this).outerWidth();

					if (itemWidth + size.temp <= size.viewport) {
						size.temp += itemWidth;
						size.visible = index;
					} else {
						return false;
					}
				});

				size.current = config.start === 0 && config.left_sided ?
					size.visible : config.start;
			};

			if (!config.disable_dots) {
				$el.dots = $carousel.find(config.dots);

				if ($el.dots.length === 0) {
					config.disable_dots = true;
				} else {
					var dotsTemplate = config.dots_template || $el.dots.html();
				}
			}

			var clickDots = function() {
				autoplay.stop();

				var page = parseInt($(this).text());

				$el.stage.gotoSlide(((size.visible + 1) * page) - 1);
			};

			var createDots = function() {
				if (!config.disable_dots) {
					$el.dots.empty();

					var countDots = Math.ceil((size.total + 1) / (size.visible + 1));

					if (countDots <= 1) {
						return false;
					}

					var d;

					for (d = 1; d <= countDots; d++) {
						var thisTemplate = dotsTemplate
								.replace(/Y/g, d);

						var $dotClone = $(thisTemplate);

						if (d === Math.ceil((size.current + 1) / (size.visible + 1))) {
							$dotClone.attr('data-selected', 'true');
						}

						$dotClone.children().click(clickDots);

						$el.dots.append($dotClone);
					}
				}
			};

			var autoplay = (function() {
				var timer = null;
				var viewport = null;

				var changeState = function(state) {
					clearInterval(timer);

					$el.carousel.attr('data-autoplay', state);
				};

				var play = function() {
					changeState('start');

					if (size.current !== size.total || config.infinite) {
						timer = setInterval(function() {
							if (size.current === size.total) {
								$el.stage.gotoSlide(size.visible);
							} else {
								$el.stage.next();
							}
						}, config.autoplay_speed);
					}
				};

				var stop = function() {
					changeState('stop');

					if (viewport != null) {
						viewport.unobserve($el.carousel[0]);
					}
				};

				var pause = function() {
					changeState('pause');
				};

				var load = function() {
					var playing = false;

					if (!$('html').hasClass('touch')) {
						changeState('pause');

						viewport = new IntersectionObserver(function(entries) {
							if (entries[0].intersectionRatio > .5) {
								playing = true;

								play();
							} else if (playing === true) {
								playing = false;

								pause();
							}
						}, {
							threshold: .5
						});

						viewport.observe($el.carousel[0]);

						$el.wrap.hover(pause, play);
					}
				};

				var init = function() {
					load();
				};

				return {
					init: init,
					play: play,
					pause: pause,
					stop: stop
				};
			})();

			if (config.autoplay) {
				autoplay.init();
			}

			var allBreakpoints = $.mq.getBreakpoints();

			$.each(allBreakpoints, function(rule) {
				if (rule === 'menu') {
					return;
				}

				$.mq.action(rule, function() {
					setSizes();

					createDots();

					if (!testInit()) {
						$el.stage.itemslide(config);
					} else {
						$el.stage.gotoSlide(size.current);
					}
				});
			});

			if (config.start === 0 && !config.infinite) {
				$el.prev.attr('disabled', 'disabled');
			}

			if (size.total <= size.visible && !config.infinite) {
				$el.next.attr('disabled', 'disabled');
			}

			var navigate = function(event) {
				autoplay.stop();

				var direction = event.data.direction;

				if (direction === 'prev') {
					if (config.infinite && (size.current - 1) < size.visible) {
						size.current = size.total;
					} else if (size.current - 1 < size.visible) {
						return false;
					} else {
						size.current--;
					}
				} else {
					if (config.infinite && (size.current + 1) > size.total) {
						size.current = size.visible;
					} else if (size.current + 1 > size.total) {
						return false;
					} else {
						size.current++;
					}
				}

				if (size.current >= size.visible && size.current <= size.total) {
					$el.stage.gotoSlide(size.current);
				}
			};

			$el.prev.click({ direction: 'prev' }, navigate);

			$el.next.click({ direction: 'next' }, navigate);

			$el.stage.on('changeActiveIndex', function() {
				size.current = $el.stage.getActiveIndex();

				if (size.current <= size.visible && !config.infinite) {
					$el.prev.attr('disabled', 'disabled');
				} else if (!config.infinite) {
					$el.prev.removeAttr('disabled');
				}

				if (size.current >= size.total && !config.infinite) {
					$el.next.attr('disabled', 'disabled');
				} else if (!config.infinite) {
					$el.next.removeAttr('disabled');
				}

				if (!config.disable_dots) {
					$el.dots.children()
						.attr('data-selected', 'false')
						.eq(Math.ceil((size.current + 1) / (size.visible + 1)) - 1)
						.attr('data-selected', 'true');
				}
			});
		};

		return this.each(function() {
			if (typeof $.fn.itemslide !== 'undefined') {
				defaultCarousel($(this));
			}
		});
	};
})(jQuery);