import $ from 'jquery';

export default class AutoFocus {
	constructor() {
		this.dataAttr = 'auto-focus';

		if (this.test()) {
			$('[data-' + this.dataAttr + ']').on('click', (e) => {
				var targetSelector = $(e.currentTarget).data(this.dataAttr);

				setTimeout(() => $(targetSelector).focus(), 10);
			});
		}
	}

	test() {
		return $('[data-' + this.dataAttr + ']').length ? true : false;
	}
}