import $ from 'jquery';

export default class Intersection {
	constructor() {
		if (this.test()) {
			this.observe();
		}
	}

	test() {
		return $('.jq_intersection').length ? true : false;
	}

	observe() {
		$('html').addClass('io-active');

		var $items = $('.jq_intersection');

		var viewport = new IntersectionObserver((items, self) => {
			$.each(items, function(i, item) {
				if (item.isIntersecting || item.intersectionRatio > 0) {
					$(item.target).addClass('io-in-view');

					self.unobserve(item.target);
				}
			});
		}, {
			threshold: .5
		});

		$items.each((i, item) => viewport.observe(item));
	}
}