import $ from 'jquery';

export default class EqualRow {
	test() {
		return typeof $.mq !== 'undefined' ? true : false;
	}

	constructor() {
		if (this.test()) {
			this.load();
		}
	}

	load() {
		$('.jq_equal_row').each((i, el) => {
			var $el = {
					row: $(el),
					columns: $('[data-equal-group]', $(el))
				};
			var groups = {};
			var breakpoint = $el.row.attr('data-breakpoint') || 'xl';

			$.mq.action(breakpoint, () => {
				$el.columns.css('height', '').each((i, el) => {
					var columnHeight = $(el).height(),
						columnGroup = $(el).attr('data-equal-group');

					if (typeof groups[columnGroup] === 'undefined') {
						groups[columnGroup] = 0;
					}

					if (columnHeight > groups[columnGroup]) {
						groups[columnGroup] = columnHeight;
					}
				});

				$.each(groups, (group, height) => {
					if (height) {
						$el.columns.filter('[data-equal-group="' + group + '"]').height(height);
					}
				});
			}, () => {
				$el.columns.css('height', '');
			});
		});
	}
}