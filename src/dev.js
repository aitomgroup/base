import jquery from 'jquery';

window.$ = window.jQuery = jquery;

require('jquery-matchmedia/dist/jquery.matchMedia');

import 'feature.js';

import 'focus-visible';

import AutoFocus from './js/auto-focus';

import EqualRow from './js/equal-row';

$(() => {
	if (typeof(window.feature) !== 'undefined') {
		window.feature.testAll();
	}

	new AutoFocus();

	new EqualRow();
});